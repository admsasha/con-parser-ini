cmake_minimum_required(VERSION 3.7)

SET(CMAKE_BUILD_TYPE Release)

PROJECT(con-parser-ini)


set (CMAKE_CXX_STANDARD 11)


add_executable(con-parser-ini main.cpp IniConfig.cpp)


install(TARGETS con-parser-ini RUNTIME DESTINATION bin)

file (GLOB SAMPLES_FILES "samples/*.*")   
install(FILES ${SAMPLES_FILES} DESTINATION share/con-parser-ini/samples/)
