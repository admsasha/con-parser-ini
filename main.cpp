#include <iostream>
#include <string.h>
#include <getopt.h>
#include <algorithm>
#include <vector>

#include "IniConfig.h"

#define VERSION "1.0.0"


void printHelp(){
    std::cout << "con-parser-ini v" VERSION"\n"
                 "utilite read ini file for bash scripts" << std::endl;
    std::cout << std::endl;

    std::cout << "Usage: " << "con-parser-ini GERERAL_OPTION ADDITIONAL_OPTIONS"  << std::endl;
    std::cout << std::endl;

    std::cout << ""
                 "-v|--version - version\n"
                 "\n"
                 "version - out version without end of line\n"
                 "\n"
                 "readAll - set bash hash\n"
                 "   -f|--filename    - filename\n"
                 "   -n|--nameHash    - hash name\n"
                 "\n"
                 "value - get value\n"
                 "   -f|--filename    - filename\n"
                 "   -s|--section     - section\n"
                 "   -k|--key         - key\n"
                 "\n"
                 "exist - check exist section\n"
                 "   -f|--filename    - filename\n"
                 "   -s|--section     - section\n"
                 "\n"
                 "groups - show all groups\n"
                 "   -f|--filename    - filename\n"
                 "   -s|--separation  - separation\n"
                 "";
    std::cout << std::endl;
}

void optionReadAll(int argc, char **argv){
    std::string filename="";
    std::string nameHash="ini";

    const char* const short_opts = "f:n:";
    const option long_opts[] = {
            {"filename", required_argument, nullptr, 'f'},
            {"nameHash", required_argument, nullptr, 'n'},
            {nullptr, no_argument, nullptr, 0}
    };

    int opt = -1;
    while((opt = getopt_long(argc, argv, short_opts, long_opts, nullptr))!=-1){
        switch (opt){
            case 'f':
                filename=std::string(optarg);
                break;
            case 'n':
                nameHash=std::string(optarg);
                break;
            case '?': // Unrecognized option
                if (optopt == 'c')
                  fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                  fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                  fprintf (stderr,
                           "Unknown option character `\\x%x'.\n",
                           optopt);
                return;
            default:
                printHelp();
                return;
        }
    }



    std::cout << "declare -A " << nameHash << std::endl;

    IniConfig config(filename);
    std::vector<std::string> groups = config.groups();
    for (std::string group:groups) {
        std::vector<std::string> keys = config.keys(group);
        for (std::string key:keys){
            std::cout << nameHash+"[\""+group+"/"+key+"\"]=\""+config.getValue(group,key,"")+"\"" << std::endl;
        }
    }
}

void optionGroups(int argc, char **argv){
    std::string filename="";
    std::string separation="\n";

    const char* const short_opts = "f:s:";
    const option long_opts[] = {
            {"filename", required_argument, nullptr, 'f'},
            {"separation", required_argument, nullptr, 's'},
            {nullptr, no_argument, nullptr, 0}
    };

    int opt = -1;
    while((opt = getopt_long(argc, argv, short_opts, long_opts, nullptr))!=-1){
        switch (opt){
            case 'f':
                filename=std::string(optarg);
                break;
            case 's':
                separation=std::string(optarg);
                break;
            case '?': // Unrecognized option
                if (optopt == 'c')
                  fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                  fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                  fprintf (stderr,
                           "Unknown option character `\\x%x'.\n",
                           optopt);
                return;
            default:
                printHelp();
                return;
        }
    }

    IniConfig config(filename);
    std::vector<std::string> groups = config.groups();
    for (size_t i=0;i<groups.size();i++) {
        std::cout << groups.at(i);
        if (i!=groups.size()-1) std::cout << separation;
    }
}

void optionExist(int argc, char **argv){
    std::string filename="";
    std::string section="";

    const char* const short_opts = "f:s:k:";
    const option long_opts[] = {
            {"filename", required_argument, nullptr, 'f'},
            {"section", required_argument, nullptr, 's'},
            {nullptr, no_argument, nullptr, 0}
    };

    int opt = -1;
    while((opt = getopt_long(argc, argv, short_opts, long_opts, nullptr))!=-1){
        switch (opt){
            case 'f':
                filename=std::string(optarg);
                break;
            case 's':
                section=std::string(optarg);
                break;
            case '?': // Unrecognized option
                if (optopt == 'c')
                  fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                  fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                  fprintf (stderr,
                           "Unknown option character `\\x%x'.\n",
                           optopt);
                return;
            default:
                printHelp();
                return;
        }
    }

    IniConfig config(filename);
    std::vector<std::string> groups = config.groups();

    if (std::find(groups.begin(), groups.end(), section)!=groups.end()){
        std::cout << "1";
    }else{
        std::cout << "0";
    }
}

void optionValue(int argc, char **argv){
    std::string filename="";
    std::string key="";
    std::string section="";
    std::string defaultValue="";

    const char* const short_opts = "f:s:k:d:";
    const option long_opts[] = {
            {"filename", required_argument, nullptr, 'f'},
            {"section", required_argument, nullptr, 's'},
            {"key", required_argument, nullptr, 'k'},
            {"default", required_argument, nullptr, 'd'},
            {nullptr, no_argument, nullptr, 0}
    };

    int opt = -1;
    while((opt = getopt_long(argc, argv, short_opts, long_opts, nullptr))!=-1){
        switch (opt){
            case 'f':
                filename=std::string(optarg);
                break;
            case 's':
                section=std::string(optarg);
                break;
            case 'k':
                key=std::string(optarg);
                break;
            case 'd':
                defaultValue=std::string(optarg);
                break;
            case '?': // Unrecognized option
                if (optopt == 'c')
                  fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                  fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                  fprintf (stderr,
                           "Unknown option character `\\x%x'.\n",
                           optopt);
                return;
            default:
                printHelp();
                return;
        }
    }

    IniConfig config(filename);
    std::cout << config.getValue(section,key,defaultValue);
}

int main(int argc, char **argv){

    if (argc<2){
        printHelp();
        return 1;
    }

    std::string general_option(argv[1]);


    if (general_option=="--version" or general_option=="-V"){
        std::cout << "version: " << VERSION << std::endl;
        return 0;
    }
    if (general_option=="version"){
        std::cout << VERSION;
    }

    if (general_option=="value"){
        optionValue(argc,argv);
        return 0;
    }

    if (general_option=="readAll"){
        optionReadAll(argc,argv);
        return 0;
    }

    if (general_option=="exist"){
        optionExist(argc,argv);
        return 0;
    }

    if (general_option=="groups"){
        optionGroups(argc,argv);
        return 0;
    }

    return 0;
}
