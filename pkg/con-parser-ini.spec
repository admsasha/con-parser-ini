Name:           con-parser-ini
Version:        1.0.0
Release:        %mkrel 1
Summary:        Utilite read ini file in bash scripts
Group:          Development/Other
License:        GPLv3+
Url:            https://bitbucket.org/admsasha/con-parser-ini
Source0:        https://bitbucket.org/admsasha/con-parser-ini/downloads/%{name}-%{version}.tar.gz

BuildRequires: cmake

%description
Utilite read ini file in bash scripts

%prep
%setup -q

%build
%cmake
%make

%install
%makeinstall_std -C build

%files
%doc COPYING
%{_bindir}/%{name}
%{_datadir}/%{name}/samples/*.ini
%attr(0755,root,root) %{_datadir}/%{name}/samples/*.sh

