#include "IniConfig.h"

#include <iostream>
#include <fstream>
#include <regex>

IniConfig::IniConfig(const std::string &filename) : filename_(filename){

}

IniConfig::~IniConfig(){

}

std::string IniConfig::getValue(const std::string &section, const std::string &key, const std::string &value_default){
    std::string value=value_default;

    std::ifstream ifs;
    ifs.open (filename_, std::ifstream::in);
    if (!ifs.is_open()){
        return value_default;
    }

    std::string line="";
    std::string curSection="";
    std::string curKey="";
    std::string curValue="";
    while (std::getline (ifs, line)){
        if (line=="") continue;

        try {
            // Поиск секции
            std::cmatch what;
            if (std::regex_match(line.c_str(), what ,std::regex("^(\\[)(\\S*)(\\])(\\s*)$"))){
                curSection=what[2];
                continue;
            }
            if (curSection!=section) continue;
 

            // Поиск ключа
            if (std::regex_match(line.c_str(), what ,std::regex("(\\S+)(\\s*)(=)([\\S\\s]*)"))){
                curKey=what[1];
                curValue=what[4];
            }
                
            if (curKey!=key) continue;

            if (curSection==section and curKey==key){
                value=curValue;
                if (std::regex_match(curValue.c_str(), what ,std::regex("^(\\s*)(\")(.*)(\")(\\s*)$"))){
                    value=what[3];
                }
                break;
            }
        } catch (std::regex_error& e) {
            //std::cout << e.what() << std::endl;
            break;
        }

    }

    ifs.close();

    return value;
}

bool IniConfig::setValue(const std::string &section, const std::string &key, const std::string &value){
    std::ifstream ifs;

    std::string newconfig = "";

    std::string line="";
    std::string curSection="";
    std::string curKey="";
    std::string curValue="";
    bool sectionFinder = false;
    bool keyFinder = false;

    ifs.open (filename_, std::ifstream::in);
    if (ifs.is_open()){
        while (std::getline (ifs, line)){
            try {
                // Поиск секции
                std::cmatch what;
                if (std::regex_match(line.c_str(), what ,std::regex("^(\\[)(\\S*)(\\])(\\s*)$"))){
                    curSection=what[2];
                }
                if (curSection==section){
                    sectionFinder=true;

                    // Поиск ключа
                    if (std::regex_match(line.c_str(), what ,std::regex("(\\S+)(\\s*)(=)([\\S\\s]*)"))){
                        curKey=what[1];
                        curValue=what[5];

                        if (curKey==key){
                            keyFinder=true;
                            newconfig.append(key+"=\""+value+"\"\n");
                            continue;
                        }
                    }
                }else{
                    if (sectionFinder==true and keyFinder==false){
                        newconfig.append(key+"=\""+value+"\"\n");
                        keyFinder=true;
                    }
                }

            } catch (std::regex_error& e) {
                //std::cout << e.what() << std::endl;
                break;
            }

            newconfig.append(line+"\n");
        }
        ifs.close();
    }

    if (sectionFinder==true and keyFinder==false){
        newconfig.append(key+"=\""+value+"\"\n");
        keyFinder=true;
    }
    if (sectionFinder==false and keyFinder==false){
        newconfig.append("\n["+section+"]\n"+key+"=\""+value+"\"\n");
    }

    std::ofstream ofs;
    ofs.open (filename_, std::ofstream::out);
    if (!ofs.is_open()){
        return false;
    }
    ofs.write(newconfig.c_str(),newconfig.length());
    ofs.close();



    return true;
}


bool IniConfig::deleteKey(const std::string &section,const std::string &key){
    std::ifstream ifs;
    ifs.open (filename_, std::ifstream::in);
    if (!ifs.is_open()){
        return false;
    }

    std::string newconfig = "";

    std::string line="";
    std::string curSection="";
    std::string curKey="";

    while (std::getline (ifs, line)){
        try {
            // Поиск секции
            std::cmatch what;
            if (std::regex_match(line.c_str(), what ,std::regex("^(\\[)(.*)(\\])$"))){
                curSection=what[2];
            }
            if (curSection==section){
                // Поиск ключа
                if (std::regex_match(line.c_str(), what ,std::regex("^(.+)([\\s|\\t]*)(=)([\\s|\\t]*)(.*)$"))){
                    curKey=what[1];
                    if (curKey==key) continue;
                }
            }
        } catch (std::regex_error& e) {
            //std::cout << e.what() << std::endl;
            break;
        }

        newconfig.append(line+"\n");
    }
    ifs.close();


    // Запись нового конфига
    std::ofstream ofs;
    ofs.open (filename_, std::ofstream::out);
    if (!ofs.is_open()){
        return false;
    }
    ofs.write(newconfig.c_str(),newconfig.length());
    ofs.close();



    return true;
}

std::string IniConfig::getValueStr(const std::string &section, const std::string &key, const std::string& value_default){
    return getValue(section,key,value_default);
}

int IniConfig::getValueInt(const std::string &section, const std::string &key, int value_default){
    return std::stoi(getValue(section,key,std::to_string(value_default)));
}

std::vector<std::string> IniConfig::groups(){
    std::vector<std::string> result;

    std::ifstream ifs;
    ifs.open (filename_, std::ifstream::in);
    if (!ifs.is_open()){
        //std::cout << "open " << filename_ << " failure" << std::endl;
        return result;
    }

    std::string line="";
    while (std::getline (ifs, line)){
        if (line=="") continue;
        try {
            // Поиск секции
            std::cmatch what;
            if (std::regex_match(line.c_str(), what ,std::regex("^(\\[)(\\S*)(\\])(\\s*)$"))){
                result.push_back(what[2]);
                continue;
            }
        } catch (std::regex_error& e) {
            //std::cout << e.what() << std::endl;
            break;
        }

    }

    ifs.close();

    return result;
}

std::vector<std::string> IniConfig::keys(const std::string &section){
    std::vector<std::string> result;

    std::ifstream ifs;
    ifs.open (filename_, std::ifstream::in);
    if (!ifs.is_open()){
        //std::cout << "open " << filename_ << " failure" << std::endl;
        return result;
    }

    std::string line="";
    std::string curSection="";
    while (std::getline (ifs, line)){
        if (line=="") continue;
        try {
            // Поиск секции
            std::cmatch what;
            if (std::regex_match(line.c_str(), what ,std::regex("^(\\[)(\\S*)(\\])(\\s*)$"))){
                curSection=what[2];
                continue;
            }
            if (curSection!=section) continue;

            // Поиск ключа
            if (std::regex_match(line.c_str(), what ,std::regex("(\\S+)(\\s*)(=)([\\S\\s]*)"))){
                result.push_back(what[1]);
            }
        } catch (std::regex_error& e) {
            //std::cout << e.what() << std::endl;
            break;
        }
    }

    ifs.close();

    return result;
}
