#ifndef INICONFIG_H
#define INICONFIG_H

#include <string>
#include <vector>

class IniConfig {
    public:
        explicit IniConfig(const std::string &filename);
        ~IniConfig();

        std::string getValue(const std::string &section, const std::string &key, const std::string& value_default="");
        bool setValue(const std::string &section, const std::string &key, const std::string &value);

        std::string getValueStr(const std::string &section, const std::string &key, const std::string& value_default="");
        int getValueInt(const std::string &section, const std::string &key, int value_default=0);

        bool deleteKey(const std::string &section, const std::string &key);

        std::vector<std::string> groups();
        std::vector<std::string> keys(const std::string &section);



    private:
        std::string filename_;
};

#endif // INICONFIG_H
